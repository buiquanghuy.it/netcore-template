﻿using NetCoreAPITemplate.Core.Domain.ComponentTypes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace NetCoreAPITemplate.Core.Domain.Components
{
    public class Product : BaseEntity
    {
        [ForeignKey("ProductType")]
        public int ProductTypeId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public bool IsActive { get; set; }
        public virtual ProductType ProductType { get; set; }
    }
}
