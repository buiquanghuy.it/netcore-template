﻿using NetCoreAPITemplate.Core.Data;
using NetCoreAPITemplate.Core.Domain.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NetCoreAPITemplate.Service.Component
{
    public class ProductService : IProductService
    {
        private IRepository<Product> _productRepo;

        public ProductService(IRepository<Product> productRepo)
        {
            _productRepo = productRepo;
        }

        public IEnumerable<Product> GetProducts()
        {
            return _productRepo.Table.ToList();
        }
    }
}
