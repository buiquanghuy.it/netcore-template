﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;
using Microsoft.Extensions.Logging;
using NetCoreAPITemplate.Core.Domain.Components;
using NetCoreAPITemplate.Service.Component;

namespace NetCoreAPITemplate.Api.Controllers
{
    [ApiVersion("1.0")]
    [ApiVersion("2.0")]
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiController]
    public class ProductController : ControllerBase
    {
        IProductService _productService;
        private readonly IStringLocalizer<ProductController> _localizer;
        private ILogger _Logger;

        public ProductController(IProductService productService, ILogger<ProductController> logger, IStringLocalizer<ProductController> localizer)
        {
            _productService = productService;
            _Logger = logger;
            _localizer = localizer;
        }

        [HttpGet]
        [Route("GetProducts")]
        [ProducesResponseType(typeof(IEnumerable<Product>), 200)]
        [ProducesDefaultResponseType]
        public IActionResult GetProducts()
        {
            _Logger.LogInformation(_localizer["Hello"]);
            var result = _productService.GetProducts();
            if (result == null) {
                return NotFound();
            }
            return Ok(result);
        }
    }
}